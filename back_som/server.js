const express = require('express');
const mymodule = require('./sum');

const server = express();

const port = 4444;

server.post('/som/:a/:b', (req, res) => {
    console.log(`/som/${req.params.a}/${req.params.b}`);
    const a = req.params.a;
    const b = req.params.b;

    const result = mymodule.sum(a, b);

    res.json({result});
});

server.listen(port, () => {
    console.log(`Server started and listening at ${port}`);
});

// to test :
// curl -X POST -H 'Content-Type: application/json' http://localhost:4444/som/5/5
// curl -X POST http://localhost:4444/som/5/5