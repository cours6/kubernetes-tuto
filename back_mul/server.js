const express = require('express');
var axios = require('axios');

const server = express();

const port = 4445;
const back_sum_port = 4444;
const back_server = process.env.SOMME_SERVEUR || "backend-somme";
console.log(back_server);
const back_sum_base_url = `http://${back_server}:${back_sum_port}/som`;

async function sum(a, b) {
    let url;
    let result = 0;
    for (let i = 0 ; i < b ; i++) {
        url = `${back_sum_base_url}/${result}/${a}`;
        const back_sum_res = await axios.post(url);
        result = back_sum_res.data.result;
    }
    return result;
}

server.post('/mul/:a/:b', (req, res) => {
    console.log(`/mul/${req.params.a}/${req.params.b}`);
    const a = req.params.a;
    const b = Number(req.params.b);
    for (let i = 0 ; i < 1000000 ; i++) {

    }
    sum(a, b)
        .then((result) => res.json({result}))
        .catch(() => res.json({"result": 0}));
});

server.listen(port, () => {
    console.log(`Server started and listening at ${port}`);
});

// to test :
// curl -X POST -H "Content-Type: application/json" http://localhost:4445/mul/3/5
// curl -X POST http://localhost:4445/mul/3/5