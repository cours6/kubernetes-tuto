import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'sumul';
  op = 'som';
  a = '0';
  b = '0';
  res = '0';

  constructor(private httpClient: HttpClient) {}

  onSubmit(form: NgForm): void {
    this.op = form.value.operator;
    this.a = form.value.a;
    this.b = form.value.b;
    console.log(form.value);

    this.httpClient
      .post(this.op + '/' + this.a + '/' + this.b, {}, {})
      .subscribe(
        (res) => {
          this.res = JSON.parse(JSON.stringify(res)).result;
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
}
